import axios from "axios";
import { useContext, useEffect, useState } from "react";
import "./App.css";
import CreateForm from "./components/CreateForm";
import TableArticles from "./components/TableArticles";
import UpdateForm from "./components/UpdateForm";
import { GlobalContext } from "./context/GlobalContext";

function App() {
  const { articles, setArticles, fetchArticles, loading, setLoading } =
    useContext(GlobalContext);

  const [edit, setEdit] = useState(false);
  const [editData, setEditData] = useState({});

  useEffect(() => {
    fetchArticles();
  }, []);

  return loading === true ? (
    <div>
      <h1>Loading...</h1>
    </div>
  ) : (
    <div className="App">
      {edit === true ? (
        <UpdateForm
          editData={editData}
          setEdit={setEdit}
          setEditData={setEditData}
        />
      ) : (
        <CreateForm />
      )}
      <TableArticles setEdit={setEdit} setEditData={setEditData} />
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          gap: "20px",
          maxWidth: "600px",
          marginLeft: "20px",
        }}
      >
        <h1>List Articles</h1>
        {articles.map((article) => {
          return (
            <div style={{ border: "2px solid black", padding: "10px" }}>
              <img src={article.image_url} style={{ width: "80px" }} alt="" />
              <h1>{article.judul}</h1>
              <p>{article.konten}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default App;
