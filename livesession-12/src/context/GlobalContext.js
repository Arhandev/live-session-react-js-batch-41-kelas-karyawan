import axios from "axios";
import { createContext, useState } from "react";

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetchArticles = async () => {
    setLoading(true);
    try {
      const response = await axios.get("http://localhost:8000/api/articles");
      setArticles(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };
  return (
    <GlobalContext.Provider
      value={{
        articles: articles,
        setArticles: setArticles,
        fetchArticles: fetchArticles,
        loading: loading,
        setLoading: setLoading,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
