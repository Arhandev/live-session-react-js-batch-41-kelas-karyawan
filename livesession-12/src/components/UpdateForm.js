import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";

function UpdateForm(props) {
  const { fetchArticles } = useContext(GlobalContext);
  const [input, setInput] = useState({
    judul: "",
    konten: "",
    image_url: "",
  });
  const handleChange = (e) => {
    if (e.target.name === "judul") {
      setInput({ ...input, judul: e.target.value });
    } else if (e.target.name === "konten") {
      setInput({ ...input, konten: e.target.value });
    } else if (e.target.name === "image_url") {
      setInput({ ...input, image_url: e.target.value });
    }
  };

  useEffect(() => {
    setInput({
      judul: props.editData.judul,
      konten: props.editData.konten,
      image_url: props.editData.image_url,
    });
  }, []);

  const onUpdate = async () => {
    try {
      const response = axios.put(
        `http://localhost:8000/api/articles/${props.editData.id}`,
        {
          judul: input.judul,
          konten: input.konten,
          image_url: input.image_url,
        }
      );
      alert("Berhasil Mengubah Artikel");
      fetchArticles();
      props.setEdit(false);
      props.setEditData({});
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div
      style={{
        border: "2px solid black",
        padding: "20px",
        borderRadius: "20px",
        maxWidth: "500px",
        margin: "20px",
      }}
    >
      <h1>Update Artikel</h1>
      <p>Judul</p>
      <input
        value={input.judul}
        onChange={handleChange}
        name="judul"
        type="text"
      />
      <p>Kontent</p>
      <input
        value={input.konten}
        onChange={handleChange}
        name="konten"
        type="text"
      />
      <p>image url</p>
      <input
        value={input.image_url}
        onChange={handleChange}
        name="image_url"
        type="text"
      />
      <div style={{ marginTop: "20px" }}>
        <button onClick={onUpdate}>Update</button>
      </div>
    </div>
  );
}

export default UpdateForm;
