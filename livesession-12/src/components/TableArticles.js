import axios from "axios";
import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalContext";

function TableArticles(props) {
  const { articles, fetchArticles } = useContext(GlobalContext);
  const deleteArticles = async (id) => {
    try {
      const response = axios.delete(`http://localhost:8000/api/articles/${id}`);
      alert("Berhasil Menghapus Artikel");
      fetchArticles();
    } catch (error) {
      console.log(error);
      alert("Gagal Menghapus Artikel");
    }
  };

  const handleEdit = (article) => {
    console.log(article);
    props.setEdit(true);
    props.setEditData(article);
  };

  return (
    <div>
      <h1>Table Artikel</h1>
      <table>
        <thead>
          <tr>
            <th>Gambar</th>
            <th>Judul</th>
            <th>Konten</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {articles.map((article, index) => {
            return (
              <tr>
                <td>
                  <img src={article.image_url} width="200" alt="" />
                </td>
                <td>{article.judul}</td>
                <td>{article.konten}</td>
                <td>
                  <div className="action">
                    <button
                      onClick={() => handleEdit(article)}
                      className="btn btn-edit"
                    >
                      Edit
                    </button>
                    <button
                      onClick={() => deleteArticles(article.id)}
                      className="btn btn-delete"
                    >
                      Delete
                    </button>
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default TableArticles;
