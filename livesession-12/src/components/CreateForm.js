import axios from "axios";
import React, { useContext, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";

function CreateForm(props) {
  const { fetchArticles } = useContext(GlobalContext);
  const [input, setInput] = useState({
    judul: "",
    konten: "",
    image_url: "",
  });

  const handleChange = (e) => {
    if (e.target.name === "judul") {
      setInput({ ...input, judul: e.target.value });
    } else if (e.target.name === "konten") {
      setInput({ ...input, konten: e.target.value });
    } else if (e.target.name === "image_url") {
      setInput({ ...input, image_url: e.target.value });
    }
  };

  const onSubmit = async () => {
    try {
      const response = await axios.post("http://localhost:8000/api/articles", {
        judul: input.judul,
        konten: input.konten,
        image_url: input.image_url,
      });
      fetchArticles();
      setInput({ judul: "", konten: "", image_url: "" });
      alert("berhasil membuat artikel");
    } catch (error) {
      console.log(error);
      alert(error.response.data.info);
    }
  };

  return (
    <div
      style={{
        border: "2px solid black",
        padding: "20px",
        borderRadius: "20px",
        maxWidth: "500px",
        margin: "20px",
      }}
    >
      <h1>Buat Artikel</h1>
      <p>Judul</p>
      <input
        name="judul"
        value={input.judul}
        onChange={handleChange}
        type="text"
      />
      <p>Kontent</p>
      <input
        name="konten"
        value={input.konten}
        onChange={handleChange}
        type="text"
      />
      <p>image url</p>
      <input
        name="image_url"
        value={input.image_url}
        onChange={handleChange}
        type="text"
      />
      <div style={{ marginTop: "20px" }}>
        <button onClick={onSubmit}>Submit</button>
      </div>
    </div>
  );
}

export default CreateForm;
