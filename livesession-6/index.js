// console.log('Halo');
// setTimeout(() => {
// 	console.log('Set Timeout 5 detik');
// }, 5000);
// setTimeout(() => {
// 	console.log('Set Timeout 7 detik');
// }, 7000);
// console.log('Line 3');

const promiseExample = age => {
	return new Promise((resolve, reject) => {
		if (age >= 17) {
			resolve('Selamat Kamu sudah dewasa');
		} else {
			reject('Kamu belum cukup umur');
		}
	});
};

const promiseType = type => {
	return new Promise((resolve, reject) => {
		if (type === 'pro') {
			resolve(`Kamu berhasil mendapatkan data`);
		} else {
			reject('Gagal mendapatkan data, harap upgrade langganan');
		}
	});
};

// promiseExample(15)
// 	.then(res => {
// 		console.log('Ini dari status fulfilled');
// 		console.log(res);
// 	})
// 	.catch(error => {
// 		console.log('Ini dari status Reject');
// 		console.log(error);
// 	});

const awaitFunction = async () => {
	try {
		let responseType = await promiseType('pro');
		console.log('Berhasil Function example');
		console.log(responseType);
	} catch (error) {
		console.log('Gagal dari promise Type');
		console.log(error);
	}
};

const awaitFunctionSecond = async () => {
	try {
		let responseAge = await promiseExample(20);
		console.log('Berhasil Function age');
		console.log(responseAge);
	} catch (error) {
		console.log('Gagal dari promise age');
		console.log(error);
	}
};

awaitFunction();
awaitFunctionSecond();
