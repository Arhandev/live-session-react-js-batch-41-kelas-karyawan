// let name = 'farhan';
// let age = 15;
// let address = 'Jalan Teratai';
// let string = `aku adalah ${name}, aku berumur ${age}, Aku tinggal di ${address}`;
// console.log(string);

// function test() {
// 	// code
// 	console.log('dari function biasa');
// }

// const arrowFunction = () => {
// 	console.log('dari arrow function');
// };

// test();
// arrowFunction();

// let arr = ['aku', 'adalah', 'farhan', 'abdul', 'hamid'];
// for (let i = 0; i < arr.length; i++) {
// 	console.log(arr[i]);
// }

// for (nilai of arr) {
// 	console.log(nilai);
// }

// let arr = ['aku', 'adalah', 'farhan', 'abdul', 'hamid'];
// let [index0, ...index] = arr;
// console.log(index0)
// console.log(index[1]);

// const obj = {
// 	name: 'farhan',
// 	address: 'Jalan Teratai',
// 	umur: 21,
// };

// const { umur, name, ...data } = obj;
// console.log(data)

// function test(...params) {
// 	console.log(params);
// }

// test('test 1', 'test 2', 'test 3', 'test 4', 'test 5');

// const obj1 = {
// 	name: 'test',
// };

// const obj2 = {
// 	kelas: 'ABC',
// 	adress: 'Jalan',
// };

// const obj = { ...obj1, ...obj2 };
// console.log(obj);

// const arr1 = [1, 2, 3];
// const arr2 = [4, 5, 6];
// const arr = [...arr1, ...arr2];

// console.log(arr)

// const arr1 = [100, 20, 5, 12, 10, 1, 2, 4];

// arr1.sort((a, b) => {
// 	return b - a;
// });
// console.log(arr1);

// const res = arr1.map(value => {
// 	return value * 3;
// });

// console.log(res);

const arr1 = [
	{
		name: 'farhan',
		domisili: 'jakarta',
	},
	{
		name: 'akbar',
		domisili: 'semarang',
	},
	{
		name: 'hanif',
		domisili: 'solo',
	},
	{
		name: 'rudi',
		domisili: 'solo',
	},
];

// const result = arr1.map(value => {
// 	return value.domisili;
// });
arr1.forEach((value, index) => {
    if(index % 2 === 0)
	console.log(index);
});
